function checkNumber(){
	var number = document.getElementById('number').value;
	var regexp = /^380(50|63|66|67|68|91|92|93|97|96)\d{7}$/;
	if(regexp.test(number)){
		document.getElementById('resultNumber').innerHTML = "<br/> Correct";
	}
	else{
		document.getElementById('resultNumber').innerHTML = "<br/> Wrong";
	}
}


function checkEmail(){
	var email = document.getElementById('email').value;
	var regexp = /^([a-zA-Z0-9_\-\.]{6,12})@(gmail\.com|rambler\.ru|mail\.ru)$/;
	if(regexp.test(email)){
		document.getElementById('resultEmail').innerHTML = "<br/> Correct";
	}
	else{
		document.getElementById('resultEmail').innerHTML = "<br/> Wrong";
	}
}



var btnNumber = document.getElementById('sendNumber');
btnNumber.addEventListener('mousedown', checkNumber);

var btnEmail = document.getElementById('sendEmail');
btnEmail.addEventListener('mousedown', checkEmail);
